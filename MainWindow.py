import sys

from PyQt5.QtWidgets import QMainWindow, QApplication

from QDialog import *

from tablawdg import Ui_MainWindow

class mainVentana(QMainWindow):

    def __init__(self):

        super(mainVentana, self).__init__()

        self.ui = Ui_MainWindow()

        self.ui.setupUi(self)

        self.ui.btnDialogo.clicked.connect(self.iniciarDialogo)

    
    def iniciarDialogo(self):

        self.main_dialog = dialogVentana()
       

if __name__ == "__main__":

    app = QApplication(sys.argv)

    ventana = mainVentana()

    ventana.show()

    sys.exit(app.exec_())