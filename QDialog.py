from PyQt5.QtWidgets import QDialog

from dialogo import Ui_Dialog

class dialogVentana(QDialog):

    def __init__(self):

        super(dialogVentana, self).__init__()

        self.ui = Ui_Dialog()

        self.ui.setupUi(self)

        self.iniciarDialog()

    def iniciarDialog(self):

        self.show()