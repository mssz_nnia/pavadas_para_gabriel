# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setMinimumSize(QtCore.QSize(800, 600))
        MainWindow.setMaximumSize(QtCore.QSize(800, 600))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.tableView = QtWidgets.QTableView(self.centralwidget)
        self.tableView.setGeometry(QtCore.QRect(40, 120, 731, 311))
        self.tableView.setMinimumSize(QtCore.QSize(731, 311))
        self.tableView.setMaximumSize(QtCore.QSize(731, 311))
        self.tableView.setObjectName("tableView")
        self.btnDialogo = QtWidgets.QPushButton(self.centralwidget)
        self.btnDialogo.setGeometry(QtCore.QRect(364, 470, 71, 23))
        self.btnDialogo.setMinimumSize(QtCore.QSize(71, 23))
        self.btnDialogo.setMaximumSize(QtCore.QSize(71, 23))
        self.btnDialogo.setObjectName("btnDialogo")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.btnDialogo.setText(_translate("MainWindow", "Dialogo"))